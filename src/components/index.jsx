import React from 'react';
import { hot } from 'react-hot-loader';
import 'normalize.css';

const App = () => <div>Hello World!</div>

export default hot(module)(App);
